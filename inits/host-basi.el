(when window-system
  ;; (set-frame-font "Fira Code-14")
  (set-frame-font "Fira Code-12")

  (defconst basi-dev-dir "/home/chrsv/dev")
  (defconst basi-iipax-one-dir (concat basi-dev-dir "/iipax-one"))
  (defconst basi-iipax-prod-dir (concat basi-dev-dir "/iipax-product"))

  (defhydra hydra-work-menu (:hint nil :color blue)
    "
Work menu

^Ripgrep^            ^Find^               ^Helm^
^-------^------------^----^---------------^----^---------
_1_: iipax one       _f_: iipax one       _p_: projectile
_2_: iipax product   _F_: iipax product   _s_: swoop
_3_: dev             ^ ^                  _i_: imenu
^ ^                  ^ ^                  _t_: tramp
^ ^                  ^ ^                  _r_: rg

"
    ("1" (lambda ()
           (interactive)
           (helm-rg-dir basi-iipax-one-dir)))
    ("2" (lambda ()
           (interactive)
           (helm-rg-dir basi-iipax-prod-dir)))
    ("3" (lambda ()
           (interactive)
           (helm-rg-dir basi-dev-dir)))
    ("f" (lambda ()
           (interactive)
           (let ((default-directory basi-iipax-one-dir))
             (helm-find nil))))
    ("F" (lambda ()
           (interactive)
           (let ((default-directory basi-iipax-prod-dir))
             (helm-find nil))))
    ("p" helm-projectile)
    ("s" helm-swoop)
    ("i" helm-imenu)
    ("t" helm-tramp)
    ("r" helm-rg)

    ("c" nil "Cancel")
    )

  (key-chord-define-global "kk" 'hydra-work-menu/body)
  (key-chord-mode +1)
  )

(require 'helm)
(require 'helm-rg)

(defun speededit (container)
  (interactive "sContainer name: ")
  (speededit-path container "/"))

(defun speededit-iipax (container)
  (interactive "sContainer name: ")
  (speededit-path container "/usr/iipax"))

(defun speededit-log (container)
  (speededit-path container "/usr/iipax/log/iipax.log"))

(defun speededit-path (container path)
  (find-file (format "/ssh:speed|docker:%s:%s" container path)))

(defun speededit-path-function (path)
  (lambda (container)
    (find-file (format "/ssh:speed|docker:%s:%s" container path))))

(defun helm-speedcontainers--make-process ()
  (let* ((grep-pattern (helm-rg--helm-pattern-to-ripgrep-regexp helm-pattern))
         (grep (if (string= "" helm-pattern) '()
                 (list "|" "grep" "-E" (concat "'" grep-pattern "'"))))
         (cmd (append '("ssh" "speed" "docker" "ps"
                        "--format" "{{.Names}}"
                        "|" "sort")
                      grep)))
      (make-process
       :name "helm-speedcontainers-proc"
       :command cmd
       :noquery t)))

(defconst helm-speedcontainers--source
  (helm-make-source "speed-docker" 'helm-source-async
    :candidates-process 'helm-speedcontainers--make-process
    :action '(("edit" . speededit)
              ("edit-iipax" . speededit-iipax)
              ("open-log" . speededit-log)))
"Source for docker containers on speed")

(defun helm-speedcontainers ()
  (interactive)
  (helm :sources '(helm-speedcontainers--source)
        :buffer "*helm speed docker source*"))

;;(setq org-plantuml-jar-path (expand-file-name "/usr/share/plantuml/plantuml.jar"))
(setq org-plantuml-jar-path "/home/chrsv/Downloads/plantuml-1.2023.10.jar")
(setq plantuml-jar-path "/home/chrsv/Downloads/plantuml-1.2023.10.jar")
(setq plantuml-default-exec-mode 'jar)
;; Doesn't work anymore
;; (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
(org-babel-do-load-languages 'org-babel-load-languages
                             '((plantuml . t)
                               (http . t)
                               (shell . t)))
(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t))) ; this line activates plantuml
