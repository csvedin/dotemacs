
(defun decode-uri (str)
  "Decodes a URI."
  (let ((decoded-str ""))
    (while (not (string-empty-p str))
      (cond ((string-match "^%[0-9a-fA-F][0-9a-fA-F]" str)
             (setq decoded-str
                   (concat decoded-str (string
                                        (string-to-number (substring str 1 3) 16))))
             (setq str (substring str 3)))
            ((string-match "^+" str)
             (setq decoded-str (concat decoded-str " "))
             (setq str (substring str 1)))
            (t
             (setq decoded-str (concat decoded-str (string (elt str 0))))
             (setq str (substring str 1)))))
    decoded-str))

(defun pretty-solr-log-line (line)
  (when (string-match "params=\{\\(.*\\)\} \\(hits=[0-9].*\\)" line)
    (let* ((encoded-params (match-string 1 line))
           (status (match-string 2 line))
           (encoded-split-params (split-string encoded-params "&"))
           (decoded-split-params (mapcar 'decode-uri encoded-split-params))
           (decoded-params (decode-uri encoded-params))
           (buffer-name "*Solr log*"))

      (let ((temp-buffer (generate-new-buffer buffer-name)))
        (with-current-buffer temp-buffer
          (insert "Params:\n\n")
          (dolist (element decoded-split-params)
            (insert (format "%s\n" element)))
          (insert "\nMeta:\n")
          (insert (format "%s\n" status)))
        (switch-to-buffer temp-buffer)))))

(defun decode-uri-region (beginning end)
  "Decodes URI-encoded components within the selected region."
  (interactive "r")
  (let ((str (buffer-substring-no-properties beginning end))
        (decoded-str (decode-uri (buffer-substring-no-properties beginning end))))
    (delete-region beginning end)
    (goto-char beginning)
    (insert decoded-str)))

(defun pretty-solr-log-line-region (beginning end)
  "Formats a solr log line so it's pretty"
  (interactive "r")
  (let ((str (buffer-substring-no-properties beginning end)))
    (pretty-solr-log-line str)))
