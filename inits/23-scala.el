
(add-to-list 'auto-mode-alist '("\\.scala\\'" . scala-mode))
(add-to-list 'auto-mode-alist '("\\.sbt\\'" . scala-mode))


(when (getenv "ENSIME_ELISP")
  ;; load the ensime lisp code...
  (add-to-list 'load-path (getenv "ENSIME_ELISP"))
  (require 'ensime)

  ;; This step causes the ensime-mode to be started whenever
  ;; scala-mode is started for a buffer. You may have to customize this step
  ;; if you're not using the standard scala mode.
  (add-hook 'scala-mode-hook 'ensime-scala-mode-hook))
