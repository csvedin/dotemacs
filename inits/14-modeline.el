(setq-default
 mode-line-format
 '(
   (:eval ml/point-position)
   (:eval ml/major-modes)
   (:eval ml/filename)
   (:eval ml/buffername)


   ;; right aligned stuff
   (:eval
    (let* ((status-offset 2)
           (nyan-offset
            (+ status-offset (if nyan-mode (+ 2 nyan-bar-length) 0)))
           (time-offset (+ nyan-offset 6)))
      (concat
       (propertize " " 'display `(space :align-to (- right ,time-offset)))
       (propertize (format-time-string "%H:%M")
              'help-echo
              (concat (format-time-string "%c; ")
                      (emacs-uptime "Uptime:%hh")))
       ;; nyan-cat
       (when (and window-system nyan-mode)
         (concat
          (propertize " " 'display `(space :align-to (- right ,nyan-offset)))
          (propertize "|" 'face 'font-lock-comment-face)
          (nyan-create)))
       (propertize "|" 'face 'font-lock-comment-face)
       ;; read-only / changed
       (propertize " " 'display `(space :align-to (- right ,status-offset)))
       (cond (buffer-read-only
              (propertize "RO" 'face 'font-lock-warning-face))
             ((buffer-modified-p)
              (propertize "* " 'face 'font-lock-warning-face))
             (t " ")))))))

(setq ml/point-position
      '(8
        " ("
        (:propertize "%l" face font-lock-string-face)
        ","
        (:eval (propertize "%c" 'face (if (>= (current-column) 80)
                                          'font-lock-warning-face
                                        'font-lock-string-face)))
        ") "))

(setq ml/major-modes
      '(:propertize "%m: " face font-lock-variable-name-face
                    help-echo buffer-file-coding-system))

;; shortened directory (if not special buffer)
(setq ml/filename
      '(:eval
        (unless (special-buffer-p (buffer-name))
          (propertize (shorten-directory default-directory 35)
                      'face 'font-lock-comment-face))))

(setq ml/buffername
      '(:propertize "%b" face font-lock-doc-face))

(defun special-buffer-p (buffer-name)
  "Check if buffer-name is the name of a special buffer. I.e.
starts and ends with *"
  (string-match-p "^\\*.+\\*$" buffer-name))

;; helper function
;; stolen from: http://amitp.blogspot.se/2011/08/emacs-custom-mode-line.html
(defun shorten-directory (dir max-length)
  "Show up to `max-length' characters of a directory name `dir'."
  (let ((path (reverse (split-string (abbreviate-file-name dir) "/")))
        (output ""))
    (when (and path (equal "" (car path)))
      (setq path (cdr path)))
    (while (and path (< (length output) (- max-length 4)))
      (setq output (concat (car path) "/" output))
      (setq path (cdr path)))
    (when path
      (setq output (concat ".../" output)))
    output))
