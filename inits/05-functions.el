
(defvar sudo-tramp-prefix
  "/sudo::"
  (concat "Prefix to be used by sudo commands when building tramp path "))

(defun sudo-file-name (filename) (concat sudo-tramp-prefix filename))

(defun sudo-reopen-file ()
  "Reopen file as root by prefixing its name with sudo-tramp-prefix and by clearing buffer-read-only"
  (interactive)
  (let*
      ((file-name (expand-file-name buffer-file-name))
       (sudo-name (sudo-file-name file-name)))
    (progn
      (setq buffer-file-name sudo-name)
      (rename-buffer sudo-name)
      (setq buffer-read-only nil)
      (message (concat "Set file name to " sudo-name)))))

(global-set-key "\C-x!" 'sudo-reopen-file)

;;; Indents and removes trailing whitespace in the whole buffer
(defun iwb ()
  "indent whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))

(defun smart-indent ()
  "Either indents selected region if it exists, otherwise whole buffer"
  (interactive)
  (if (use-region-p)
      (indent-region (region-beginning) (region-end))
    (iwb)))

(defun helm-rg-dir (path)
  (interactive)
  (let ((helm-rg-default-directory path))
    (helm-rg "")))

(defun delete-lines-before-point ()
  "Deletes all lines from top to point"

  (interactive)
  (let ((startPos (point)))
    (forward-line 0)
    (let ((lineLength (- startPos (point))))
      (delete-region 1 (point))
      (goto-char (+ 1 lineLength)))))

;; C-a moves point to the first non whitespace character. If already on
;; first character, move point to the beginning of line
(defun move-point-to-beginning ()
  (interactive)
  (let ((oldpos (point)))
    (back-to-indentation)
    (and (= oldpos (point))
         (beginning-of-line))))

(defun hard-revert-buffer ()
  (interactive)
  (revert-buffer t t))

(defun hostname-p (hostname)
  (let ((hn (downcase (car (split-string system-name "\\.")))))
    (equal hn hostname)))

(defun display-ansi-colors ()
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))

(global-set-key (kbd "C-M-<backspace>") 'delete-lines-before-point)
(global-set-key (kbd "C-<tab>") 'smart-indent)
(global-set-key (kbd "C-a") 'move-point-to-beginning)
(global-set-key (kbd "M-r") 'hard-revert-buffer)
