
(when (hostname-p "captain")
  (add-to-list 'load-path (concat config-dir "plugins/vtl-mode"))
  (require 'vtl)

  (add-to-list 'load-path (concat config-dir "plugins/org-exporters"))
  (require 'ox-confluence)

  (defun shell-dir (name dir)
    (interactive "sShell name: \nDDirectory: ")
    (let ((default-directory dir))
      (shell name)))

  (defun build-buffers()
    (interactive)

    (shell-dir "build-lims" "d:/dev/lims/")
    (shell-dir "build-forum" "d:/dev/forum_limbo/")
    (shell-dir "build-fm" "d:/dev/fm/")
    (shell-dir "build-rgs" "d:/dev/rgs/")
    (shell-dir "build-dynamo" "d:/dev/dynamo/")
    (shell-dir "build-upgrade" "d:/dev/upgrade/"))

  (defun build-buffers-cygwin()
    (interactive)

    (shell-dir "build-lims" "/cygdrive/f/dev/lims/")
    (shell-dir "build-forum" "/cygdrive/f/dev/forum_limbo/")
    (shell-dir "build-fm" "/cygdrive/f/dev/fm/")
    (shell-dir "build-rgs" "/cygdrive/f/dev/rgs/")
    (shell-dir "build-upgrade" "/cygdrive/f/dev/upgrade/")))
